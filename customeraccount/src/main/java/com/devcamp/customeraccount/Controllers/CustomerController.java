package com.devcamp.customeraccount.Controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.customeraccount.models.Customer;
import com.devcamp.customeraccount.services.CustomerService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CustomerController {
    @Autowired
    CustomerService customerService;
    @GetMapping("/customers")
    public ArrayList<Customer> getAllCustomers(){
        return customerService.getAllCustomers();
    }

    
}
